const { Router } = require("express");
const { getValue , getValues , setValue , delValue , updateValue } = require("../db/controller");
const app = Router();

const test = require("../config");

app.get("/",getValues);
app.get("/:id",getValue);
app.post("/",setValue);
app.put("/:id",updateValue);
app.delete("/:id",delValue);

module.exports = app;